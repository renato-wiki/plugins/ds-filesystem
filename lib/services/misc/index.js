/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const fs = require("fs");
const fse = require("fs-extra");
const path = require("path");
const Promise = require("bluebird");

const fsAccess = Promise.promisify(fs.access);
const fsReadFile = Promise.promisify(fs.readFile);
const fsWriteFile = Promise.promisify(fs.writeFile);

const fsEnsureDir = Promise.promisify(fse.ensureDir);

/*===================================================== Exports  =====================================================*/

exports.write = write;
exports.read = read;

/*==================================================== Functions  ====================================================*/

function write(basePath, module, key, value) {
  let file = path.join(basePath, module, key);
  let directory = path.dirname(file);
  return fsEnsureDir(directory)
      .then(() => fsWriteFile(file, value));
}

function read(basePath, module, key) {
  let file = path.join(basePath, module, key);
  return fsAccess(file, fs.R_OK)
      .then(
          () => fsReadFile(file),
          () => void 0
      );
}
