/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");

const fsReadDir = Promise.promisify(fs.readdir);
const fsWriteFile = Promise.promisify(fs.writeFile);
const fsStat = Promise.promisify(fs.stat);
const fsReadFile = Promise.promisify(fs.readFile);

const INDEX_FILE_NAME = "index.json";

/*
 * RIO := category
 *
 * category  := {id, type, uri, meta, children} // hidden: {parent, path, pageMap, categoryMap, subCategoryMeta,
 *                                              //          rebuild, persist}
 * page      := {id, type, uri, meta}           // hidden: {parent, path, ext}
 * children  := {categories[], pages[]}
 * meta      := {title, ...}
 */

const CAT = "category", PAG = "page";
const PLURAL = {[CAT]: "categories", [PAG]: "pages"};

/*===================================================== Exports  =====================================================*/

module.exports = build;

/*==================================================== Functions  ====================================================*/

function build(dataKey, renato, config) {
  const sortProps = config.sort, extData = {extensions: renato.extensions, handlers: renato.handlers};
  const rootDir = path.resolve(renato.config.paths.base, config.paths.content);
  return createCategory(sortProps, extData, dataKey, rootDir, null, "", rootDir, "/", {title: "Overview"})
      .then((index) => { return {index}; });
}

/*----------------------------------------------------- category -----------------------------------------------------*/

function fsReadCategory(categoryDir) {
  return Promise.all([
    fsReadFile(path.join(categoryDir, INDEX_FILE_NAME)).then(JSON.parse, _.constant({})),
    fsReadDir(categoryDir)
  ]);
}

function createCategory(sortProps, extData, dataKey, rootDir, parentIndexData, uri, dir, id, meta) {
  const data = {
    parent: parentIndexData, path: dir, pageMap: {}, categoryMap: {}, subCategoryMeta: null, rebuild, persist
  };
  const index = {id, type: CAT, uri, meta, [dataKey]: data};
  data.index = {nested: index, flat: {id, type: CAT, uri, meta, [dataKey]: data}};

  return createChildren(data, sortProps, extData, dataKey, rootDir, uri)
      .then((result) => {
        data.subCategoryMeta = result.subCategoryMeta;
        index.children = result.children;
        return index;
      });

  function rebuild() {
    return createCategory(sortProps, extData, dataKey, rootDir, parentIndexData, uri, dir, id, meta)
        .then((newIndex) => {
          const newData = newIndex[dataKey];
          _.assign(index, newIndex);
          _.assign(data, newData);
          return index;
        });
  }

  function persist() { return fsWriteFile(path.join(dir, INDEX_FILE_NAME), JSON.stringify(data.subCategoryMeta)); }
}

/*------------------------------------------------------- page -------------------------------------------------------*/

function createPage(extData, dataKey, parentIndexData, parentURI, file, filename) {
  const ext = _.find(extData.extensions, (ext) => filename.endsWith(ext));
  // ignore files that don't match any registered extension
  if (ext == null) { return null; }
  // create index and data
  const id = path.basename(filename, ext);
  const uri = path.join(parentURI, id);
  const meta = {title: id};
  const data = {parent: parentIndexData, path: file, ext};
  const index = {id, type: PAG, uri, meta, [dataKey]: data};
  data.index = {nested: index, flat: {id, type: PAG, uri, meta, [dataKey]: data}};

  let handlerKey = "core::index:update:build:" + ext;
  if (!extData.handlers.hasOwnProperty(handlerKey)) { return index; }
  return Promise
      .mapSeries(extData.handlers[handlerKey], (handler) => handler.fn(index, file))
      .then(_.constant(index));
}

/*----------------------------------------------------- children -----------------------------------------------------*/

function createChildren(indexData, sortProps, extData, dataKey, rootDir, uri) {
  return fsReadCategory(path.resolve(rootDir, uri))
      .then((result) => {
        const subCategoryMeta = result[0], files = result[1];
        return Promise
            .map(files, _.partial(createChild, sortProps, extData, dataKey, rootDir, subCategoryMeta, indexData, uri))
            .then(_.partial(groupAndSortChildren, sortProps, indexData))
            .then((children) => { return {children, subCategoryMeta}; });
      });
}

function createChild(sortProps, extData, dataKey, rootDir, subCategoryMeta, indexData, uri, filename) {
  let fileRel = path.join(uri, filename);
  let file = path.join(rootDir, fileRel);
  return fsStat(file)
      .then((stats) => {
        if (stats.isDirectory()) {
          const meta = _.extend({title: filename}, subCategoryMeta[filename]);
          return createCategory(sortProps, extData, dataKey, rootDir, indexData, fileRel, file, filename, meta);
        } else if (filename !== INDEX_FILE_NAME) {
          return createPage(extData, dataKey, indexData, uri, file, filename);
        }
      });
}

function groupAndSortChildren(sortProps, indexData, childDataList) {
  const duplicateIds = {};
  const childCategories = [], childPages = [];
  _.each(childDataList, (childIndex) => {
    if (childIndex == null) { return; }
    const childrenOfTypeList = childIndex.type === CAT ? childCategories : childPages;
    const childrenOfTypeMap = indexData[childIndex.type + "Map"];
    deduplicateId(duplicateIds, childIndex, childrenOfTypeMap);
    childrenOfTypeMap[childIndex.id] = childIndex;
    childrenOfTypeList.push(childIndex);
  });
  return {
    [PLURAL[CAT]]: sortProps[CAT] ? _.sortBy(childCategories, sortProps[CAT]) : childCategories,
    [PLURAL[PAG]]: sortProps[PAG] ? _.sortBy(childPages, sortProps[PAG]) : childPages
  };
}

/*------------------------------------------------------ utils  ------------------------------------------------------*/

function deduplicateId(duplicateIds, index, map) {
  const originalId = index.id;
  const suffix = "." + index.type[0];
  if (map.hasOwnProperty(originalId)) {
    let dupIndex = map[originalId];
    dupIndex.id = originalId + suffix + 0;
    map[dupIndex.id] = dupIndex;
    Reflect.deleteProperty(map, originalId);
    duplicateIds[originalId] = 0;
  }
  if (duplicateIds.hasOwnProperty(originalId)) { index.id = originalId + suffix + (++duplicateIds[originalId]); }
}
