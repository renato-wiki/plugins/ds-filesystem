/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");

const rio = require("./services/renato-index-object/index");
const misc = require("./services/misc/index");

const PACKAGE = require("../package.json");
const DEFAULT_CONFIG = require("./constants/default-config");

const fsMkDir = Promise.promisify(fs.mkdir);
const fsWriteFile = Promise.promisify(fs.writeFile);

/*===================================================== Exports  =====================================================*/

module.exports = Plugin;

/*==================================================== Functions  ====================================================*/

function Plugin(renato, config) {
  this.renato = renato;
  this.config = _.defaultsDeep({}, config, DEFAULT_CONFIG);
  this.config.paths = _.mapValues(this.config.paths, (p) => path.resolve(renato.config.paths.base, p));
  this.log = renato.log.child({plugin: {name: PACKAGE.name, version: PACKAGE.version}});
}

// misc data management

Plugin.prototype.writeData = function (data) { /* {module, key, value} */
  let payload = {module: data.module, key: data.key};
  this.log.debug("write requested", payload);
  return misc
      .write(this.config.paths.misc, data.module, data.key, data.value)
      .tap(() => this.log.debug("write succeeded", payload))
      .return(data);
};

Plugin.prototype.readData = function (data) { /* {module, key} */
  let payload = {module: data.module, key: data.key};
  this.log.debug("read requested", payload);
  return misc
      .read(this.config.paths.misc, data.module, data.key)
      .tap(() => this.log.debug("read succeeded", payload))
      .then((result) => {
        data.value = result;
        return data;
      });
};

// rio management todo move everything but logging into service

Plugin.prototype.buildIndex = function (data) { /* {dataKey} */
  return rio
      .build(data.dataKey, this.renato, this.config)
      .then((result) => _.assign(data, result));
};

Plugin.prototype.retrieveIndex = function (data) { /* {dataKey, rootIndex, path} */
  return _.assign(data, rio.retrieve(data.dataKey, data.rootIndex, data.path));
};

Plugin.prototype.createCategory = function (data) { /* {parentRIOData, id, meta} */
  let id = data.id, parentRIOData = data.parentRIOData;
  parentRIOData.subCategoryMeta[id] = data.meta;
  return parentRIOData
      .persist()
      .then(() => fsMkDir(path.join(parentRIOData.path, id)))
      .then(parentRIOData.rebuild);
};

Plugin.prototype.updateCategory = function (data) { /* {rioData, [meta]} */
  let id = data.rioData.id, parentRIOData = data.rioData.parent;
  if (_.matches(parentRIOData.subCategoryMeta[id])(data.meta)) { return; }
  _.assign(parentRIOData.subCategoryMeta[id], data.meta);
  return parentRIOData
      .persist()
      .then(parentRIOData.rebuild);
};

Plugin.prototype.createPage = function (data) { /* {parentRIOData, id, content} */
  let parentRIOData = data.parentRIOData;
  return fsWriteFile(path.join(parentRIOData.path, data.id), data.content)
      .then(parentRIOData.rebuild);
};

Plugin.prototype.updatePage = function (data) { /* {rioData, [content]} */
  let parentRIOData = data.rioData.parent;
  if (data.content == null) { return; }
  return fsWriteFile(data.rioData.path, data.content)
      .then(parentRIOData.rebuild);
};
